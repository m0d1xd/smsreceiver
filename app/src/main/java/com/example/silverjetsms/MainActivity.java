package com.example.silverjetsms;

import androidx.appcompat.app.AppCompatActivity;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.example.silverjetsms.services.MySmsReceiver;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.DexterError;
import com.karumi.dexter.listener.PermissionDeniedResponse;
import com.karumi.dexter.listener.PermissionGrantedResponse;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.PermissionRequestErrorListener;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;

import java.util.List;

public class MainActivity extends AppCompatActivity {
    private static final String TAG = "MoDi";

    private TextView tv_permission, tv_request;


    private TextView tv_read_permission, tv_receive_permission, tv_boot_permission;

    private PermissionRequestErrorListener errorListener;
    private MultiplePermissionsListener allPermissionsListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        tv_permission = findViewById(R.id.tv_permission);
        tv_request = findViewById(R.id.tv_request);
        tv_read_permission = findViewById(R.id.tv_read_permission);
        tv_receive_permission = findViewById(R.id.tv_receive_permission);
        tv_boot_permission = findViewById(R.id.tv_boot_permission);

        createPermissionListener();

        tv_request.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Dexter.withActivity(MainActivity.this)
                        .withPermissions(Manifest.permission.READ_SMS, Manifest.permission.RECEIVE_SMS)
                        .withListener(allPermissionsListener)
                        .check();
            }
        });

        Dexter.withActivity(this)
                .withPermissions(Manifest.permission.READ_SMS,
                        Manifest.permission.RECEIVE_SMS,
                        Manifest.permission.RECEIVE_BOOT_COMPLETED)
                .withListener(allPermissionsListener)
                .check();

        MySmsReceiver smsReceiver = new MySmsReceiver();
        IntentFilter mIntentFilter = new IntentFilter();
        mIntentFilter.addAction("android.provider.Telephony.SMS_RECEIVED");

        this.registerReceiver(smsReceiver, mIntentFilter);

    }

    private void createPermissionListener() {
        allPermissionsListener = new MultiplePermissionsListener() {
            @Override
            public void onPermissionsChecked(MultiplePermissionsReport report) {
                for (PermissionGrantedResponse permissionGrantedResponse : report.getGrantedPermissionResponses()) {
                    Log.d(TAG, "permissionGrantedResponse: " + permissionGrantedResponse.getPermissionName());
                    hideInto(permissionGrantedResponse.getPermissionName());

                }
                if (report.getDeniedPermissionResponses().size() == 0) {
                     tv_request.setVisibility(View.INVISIBLE);
                    tv_permission.setVisibility(View.INVISIBLE);
                }
                for (PermissionDeniedResponse permissionDeniedResponse : report.getDeniedPermissionResponses()) {
                    Log.d(TAG, "permissionDeniedResponse: " + permissionDeniedResponse.getPermissionName());
                    showInfo(permissionDeniedResponse.getPermissionName());


                }
            }

            @Override
            public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, final PermissionToken token) {
                new AlertDialog.Builder(MainActivity.this)
                        .setTitle(R.string.permission_rationale_title)
                        .setMessage(R.string.permission_rationale_message)
                        .setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                                token.cancelPermissionRequest();
                            }
                        })
                        .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                                token.continuePermissionRequest();
                            }
                        })
                        .setOnDismissListener(new DialogInterface.OnDismissListener() {
                            @Override
                            public void onDismiss(DialogInterface dialog) {
                                token.cancelPermissionRequest();
                            }
                        })
                        .show();
            }
        };
        errorListener = new PermissionRequestErrorListener() {
            @Override
            public void onError(DexterError error) {
                Log.i(TAG, "onError: " + error.toString());
            }
        };

    }

    //    private TextView tv_read_permission, tv_receive_permission, tv_boot_permission;
    private void hideInto(String permission) {
        switch (permission) {
            case Manifest.permission.READ_SMS:
                tv_read_permission.setVisibility(View.INVISIBLE);
                break;
            case Manifest.permission.RECEIVE_SMS:
                tv_receive_permission.setVisibility(View.INVISIBLE);
                break;
            case Manifest.permission.RECEIVE_BOOT_COMPLETED:
                tv_boot_permission.setVisibility(View.INVISIBLE);
                break;
            default:
                break;

        }

    }

    private void showInfo(String permission) {
        switch (permission) {
            case Manifest.permission.READ_SMS:
                tv_read_permission.setVisibility(View.VISIBLE);
                break;
            case Manifest.permission.RECEIVE_SMS:
                tv_receive_permission.setVisibility(View.VISIBLE);
                break;
            case Manifest.permission.RECEIVE_BOOT_COMPLETED:
                tv_boot_permission.setVisibility(View.VISIBLE);
                break;
            default:
                break;

        }

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void onStart() {
        super.onStart();

    }
}
