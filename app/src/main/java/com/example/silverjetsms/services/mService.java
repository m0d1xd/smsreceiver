package com.example.silverjetsms.services;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

import com.example.silverjetsms.MainActivity;

public class mService extends Service {
    @Override
    public IBinder onBind(Intent arg0) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        handleCommand(intent);
        return Service.START_STICKY;
    }

    private void handleCommand(Intent intent) {
        if (Intent.ACTION_BOOT_COMPLETED.equals(intent.getAction())) {
            Intent i = new Intent(getApplicationContext(), MainActivity.class);
            i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(i);
        }
    }


    @Override
    public void onCreate() {
        super.onCreate();
    }

}
