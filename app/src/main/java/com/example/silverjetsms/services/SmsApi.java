package com.example.silverjetsms.services;


import com.example.silverjetsms.models.ApiResponse;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface SmsApi {

    @Headers("Content-Type:application/json")

    @POST("sms3.php")
    Call<ApiResponse> postSms(@Query("key") String key,
                         @Query("message") String message,
                         @Query("phone") String phone);

    @POST("sms3.php")
    Call<String> postSms(
            @Query("message") String message,
            @Query("phone") String phone);

}
